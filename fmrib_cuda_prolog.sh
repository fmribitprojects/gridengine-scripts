#!/bin/bash

# Exit states:
SUCCESS=0 # Success
RESHED=99 # Re-queue if FORBID-RESCHEDULE not set
APPERR=100 # Re-queue if FORBID-APPERROR not set
HOSTERR=101 # Other - Set queue to error state and resubmit job

CONF=/etc/sysconfig/cuda.conf

grid_owned='0'

if [ -f "${CONF}" ]; then
    source "${CONF}"
fi

DEVMODE=0660
DEVGROUP=root
SMI=/usr/bin/nvidia-smi
LOCKDIR=/var/run/lock/cuda
LOCKGPU=1
MAIL=/usr/bin/mail
ADMIN=manager@fmrib.ox.ac.uk
VARS=''
HOST=$(hostname -s)

mkdir -p ${LOCKDIR}

function gpu_used() {
    gpu_id=$1
    lockfile="${LOCKDIR}/gpu${gpu_id}"
    if [ -f "$lockfile" ]; then
        job=$(awk '{ print $2 }' < "$lockfile")
        # Look for sge_shepherd job
        if pidof "sge_shepherd-$job" > /dev/null; then
            # Job is running
            return 0
        fi
        # No shepherd matching the lock file, clean it up
        echo "Dangling lockfile for gpu $gpu_id, cleaning up"
        unlock_gpu "$gpu_id"
    fi
    return 1
}


function unlock_gpu() {
    gpu_id=$1
    /usr/bin/chgrp ${DEVGROUP} "/dev/nvidia${gpu_id}"
    /usr/bin/chmod ${DEVMODE} "/dev/nvidia${gpu_id}"
    /usr/bin/rm -f "${LOCKDIR}/gpu${gpu_id}"
}

function mail_admin() {
    echo "$1" | ${MAIL} -s "CUDA Job prolog failure" ${ADMIN}
}

function number_in_list() {
    number=$1
    shift
    list=$*

    for i in ${list}; do
        if [ "${number}" -eq "${i}" ]; then
            return 0
        fi
    done
    return 1
}

# Identify cuda devices
if [ -x ${SMI} ]; then
    CUDA=1
    devices=$(${SMI} --query-gpu=uuid --format=csv,noheader)
    DRIVER_FAILED=$?
    if [ ${DRIVER_FAILED} -ne 0 ]; then
        # NVIDIA Driver not functioning correctly
        mail_admin "Unable to run ${SMI} - NVIDIA driver probably not functioning on ${HOST}"
        exit ${HOSTERR}
    fi
else
    CUDA=0
fi

[ "$SGE_TASK_ID" = "undefined" ] && SGE_TASK_ID=1
SGE_GROUP=$(/usr/bin/awk -F'=' '/^add_grp_id/{print $2}' ${SGE_JOB_SPOOL_DIR}/config)

function clean_exit() {
    code=$1
    shift
    gpu_list=$*
    if [ ${LOCKGPU} -eq 1 ]; then
        for gpu in ${gpu_list}; do
            unlock_gpu "${gpu}"
        done
    fi
    exit ${code}
}

if [ $CUDA -eq 1 ]; then
    export CUDA_DEVICE_ORDER="PCI_BUS_ID"
    OWNED_GPU_LIST=()
    # Find next available GPU
    for gpu in ${devices}; do
        minor=$(${SMI} -q -i "${gpu}" | /usr/bin/grep "Minor" | /usr/bin/cut -d: -f2 | /usr/bin/sed 's/ //g')
        if ! number_in_list "${minor}" ${grid_owned}; then
            continue
        fi
        if [ -z "$(${SMI} --query-compute-apps=pid -i "${gpu}" --format=csv,noheader)" ]; then
            # GPU is currently free - check lock file
            if gpu_used "${minor}"; then
                # GPU lock file matches running job (will clean up bad lock files)
                continue
            fi
            echo "${SGE_O_LOGNAME} ${JOB_ID} ${SGE_TASK_ID} ${minor} ${gpu} ${SGE_GROUP} ${LOCKGPU}" > "${LOCKDIR}/gpu${minor}"

            if [ ${LOCKGPU} -eq 1 ]; then
                /usr/bin/chmod "${DEVMODE}" "/dev/nvidia${minor}"
                /usr/bin/chgrp "${SGE_GROUP}" "/dev/nvidia${minor}"
            fi
            OWNED_GPU_LIST+=("${gpu}")
            OWNED_DEV_LIST+=("${minor}")
            if [ "${#OWNED_GPU_LIST[@]}" -eq "${NSLOTS}" ]; then
                break
            fi
        fi
    done
    if [ "${#OWNED_GPU_LIST[@]}" -ne "${NSLOTS}" ]; then
        echo "Unable to schedule requested number of GPUs." >&2
        mail_admin "SGE gpu resource does not match available devices on ${HOSTNAME} - ${SGE_O_LOGNAME} ${JOB_ID} ${SGE_TASK_ID} ${minor} ${gpu} ${SGE_GROUP} ${LOCKGPU}"
        # Failed to get required GPUs - reshedule the job
        clean_exit ${HOSTERR} "${OWNED_DEV_LIST[@]}"
    fi

    SGE_GPU=$(echo "${OWNED_GPU_LIST[*]}" | /usr/bin/sed 's/ /,/g')
    CUDA_VISIBLE_DEVICES=${SGE_GPU}
    VARS="${VARS} CUDA_DEVICE_ORDER SGE_GPU CUDA_VISIBLE_DEVICES"
fi

# Create MATLAB Runtime Cache configuration
MCR_CACHE_ROOT="/tmp/mcr_${JOB_ID}-${SGE_TASK_ID}"
VARS="${VARS} MCR_CACHE_ROOT"

# Set the following Env. Vars
for env in ${VARS}; do
    echo "$env="`eval echo "\\$$env"` >> "${SGE_JOB_SPOOL_DIR}/environment"
done