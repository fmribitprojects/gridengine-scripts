#!/bin/bash

# Clean up MCR caches

# mcr_${JOB_ID} or mcr_${JOB_ID}-${TASK_ID}

cache_folder=/tmp
host=`hostname -s`
grid_dir=/var/spool/sge/${host}
active_jobs=${grid_dir}/active_jobs

# Sanitise PATH
PATH=/usr/bin:/bin

for cache in "${cache_folder}"/mcr_*; do
    jobdesc=$(echo "${cache}" | cut -d_ -f2)
    jobid=$(echo "${jobdesc}" | cut -d- -f1)
    taskid=$(echo "${jobdesc}" | cut -d- -f2)
    if [ -z "${taskid}" ]; then
        taskid=1
    fi
    if [ ! -d "${active_jobs}/${jobid}.${taskid}" ]; then
        # Validate the cache folder before issuing a delete
        echo "${cache}" | grep -E "^${cache_folder}/mcr_[0-9]+-[0-9]+$" >/dev/null 2>&1
        if [ $? -eq 0 ]; then
            /usr/bin/rm -rf "${cache}"
        fi
    fi
done