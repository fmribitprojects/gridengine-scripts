#!/bin/bash
VARS=''

[ "$SGE_TASK_ID" = "undefined" ] && SGE_TASK_ID=1

# Create MATLAB Runtime Cache configuration
MCR_CACHE_ROOT="/tmp/mcr_${JOB_ID}-${SGE_TASK_ID}"
VARS="${VARS} MCR_CACHE_ROOT"

# Set the following Env. Vars
for env in ${VARS}; do
    echo "$env="`eval echo "\\$$env"` >> ${SGE_JOB_SPOOL_DIR}/environment
done