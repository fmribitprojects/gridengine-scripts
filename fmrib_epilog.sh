#!/bin/bash
RM=/usr/bin/rm

co_maj=$(awk '{ print $3 }' /etc/centos-release | cut -d. -f1)
if [[ "$co_maj" = "6" ]]; then
    RM=/bin/rm
fi

[ "$SGE_TASK_ID" = "undefined" ] && SGE_TASK_ID=1

# Delete MATLAB Runtime Cache configuration
${RM} -rf "/tmp/mcr_${JOB_ID}-${SGE_TASK_ID}"
