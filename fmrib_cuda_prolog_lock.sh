#!/bin/bash
this_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
flock /var/run/lock/cuda_prolog -c "${this_dir}/fmrib_cuda_prolog.sh"