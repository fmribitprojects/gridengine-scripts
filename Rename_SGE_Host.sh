#!/bin/bash

stage=$1
oh=$2
nh=$3
if [[ "$stage" != "remove" && "$stage" != "add" && "$stage" != "both" ]]; then
	echo "Rename_SGE_Host.sh [remove|add|both] oldhostname newhostname"
        exit 1
fi

if [[ -z $oh ]] | [[ -z $nh ]]; then
	echo "Rename_SGE_Host.sh [remove|add|both] oldhostname newhostname"
	exit 1
fi

for h in $oh $nh; do
	if ! host $h; then
		echo "$h not found in DNS"
		exit 2
	fi
done

qroot=/opt/sge/default/spool/qmaster/cqueues


queues=$(qconf -sql)

if [[ "$stage" = "remove" || "$stage" = "both" ]]; then
	echo "Capturing host config"
	mkdir -p ~/.tmp_$nh
	pushd ~/.tmp_$nh
	# Capture queue configs
	for q in $queues; do
		qconf -sq $q | sed ':a;N;$!ba;s/ \\\n//g' | tr -s " " | sed 's/, /,/g' | sed "s/$oh/$nh/g"  > $q
		qconf -sq $q | sed ':a;N;$!ba;s/ \\\n//g' | tr -s " " | sed 's/, /,/g' | sed "s/,\[$oh=[^]]*\]//" | sed "s/$oh//g" > $q.clean
	done

	# Capture hostgroups
	hgs=$(qconf -shgrpl)
	for hg in $hgs; do
		qconf -shgrp "$hg" | sed "s/$oh/$nh/g" > "$hg"
		qconf -shgrp "$hg" | sed ':a;N;$!ba;s/ \\\n//g' | tr -s " "| sed "s/$oh//" > "$hg.clean"
	done

	# Capture the host config
	qconf -se $oh | sed ':a;N;$!ba;s/, \\\n/,/g' | grep -v load_values | grep -v processors| sed "s/$oh/$nh/g" > $nh
	# Capture the host options
	qconf -sconf $oh | sed "s/$oh/$nh/g" > $nh.sconf

        
	# Remove from host groups
	for hg in $hgs; do
        	qconf -Mhgrp "$hg.clean"
	done

	# Remove from queues
	for q in $queues; do
		qconf -Mq $q.clean
	done

	echo "Removing old host"
	qconf -de $oh
	qconf -dh $oh
        qconf -dconf $oh

	if [[ "$stage" = "remove" ]]; then
		popd
	fi
fi

if [[ "$stage" = "add" || "$stage" = "both" ]]; then
	if [[ ! -d "$HOME/.tmp_$nh" ]]; then
		echo "Cannot find configuration to add"
		exit 3
        fi
	cd "$HOME/.tmp_$nh"
	# Create new host
	echo "Adding $nh as admin host"
	qconf -ah $nh

	echo "Adding $nh as execution host"
	cat $nh
	qconf -Ae $nh

	echo "Configuring host"
	cat $nh.sconf
	mv $nh.sconf $nh
	qconf -Aconf $nh

	echo "Adding to Host groups"
	for hg in $hgs; do
        	qconf -Mhgrp "$hg" 
	done

	echo "Updating queue configs"
	for q in $queues; do
		qconf -Mq $q
	done

	popd
	rm -rf ~/.tmp_$$
fi
