#!/bin/bash

if [ $UID -gt 199 ] && [ "`/usr/bin/id -gn`" = "`/usr/bin/id -un`" ]; then
    umask 002
else
    umask 022
fi

case "$SGE_STARTER_SHELL_START_MODE" in

    unix_behavior)
        exec "$@" ;;

#
# Although  posix_compliant and script_from_stdin are the same, the behavior is different:
# posix_compliant => $1 is the name of the script
# script_from_stdin => $1 is -s (option to the shell to read from stdin)
#

    posix_compliant | script_from_stdin)
        if [ "$SGE_STARTER_USE_LOGIN_SHELL" = "true" ]; then
            exec -l -a "${SGE_STARTER_SHELL_PATH##*/}" "$SGE_STARTER_SHELL_PATH" "${@}"
        else
            exec -a "${SGE_STARTER_SHELL_PATH##*/}" "$SGE_STARTER_SHELL_PATH" "${@}"
        fi ;;

esac
exit 100
